#ifndef BINARYTREE_H
#define BINARYTREE_H

struct BinaryTreeNode;
struct BinaryTreeData;
class BinaryTree
{
public:
    BinaryTree();
    BinaryTreeNode *root() const;

    void preOrderTraversal(BinaryTreeNode *node) const;
    void inOrderTraversal(BinaryTreeNode *node) const;
    void postOrderTraversal(BinaryTreeNode *node) const;
    void levelOrderTraversal(BinaryTreeNode* node) const;
    int SizeOfBinaryTree(BinaryTreeNode* root) const;
    int HeightOfTree(BinaryTreeNode* root) const;

private:
    BinaryTreeData* d;
};

#endif // BINARYTREE_H
