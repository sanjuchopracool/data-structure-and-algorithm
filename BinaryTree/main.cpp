#include <iostream>
#include <BinaryTree.h>
using namespace std;

int main()
{
    BinaryTree tree;
    tree.levelOrderTraversal(tree.root());
    cout << endl;
    cout <<  "Size of Binary Tree: " << tree.SizeOfBinaryTree(tree.root()) << endl;
    cout <<  "Height of Binary Tree: " << tree.HeightOfTree(tree.root()) << endl;
    return 0;
}

