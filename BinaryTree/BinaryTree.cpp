#include <iostream>
#include "BinaryTree.h"
#include <QString>
#include <QList>
#include<QQueue>
using namespace std;
struct BinaryTreeNode
{
    BinaryTreeNode(QString val) : data(val), left(0), right(0) {}
    QString data;
    BinaryTreeNode* left;
    BinaryTreeNode* right;

    void print() {
        cout << data.toStdString();
    }
};

struct BinaryTreeData
{
    BinaryTreeData() : root(0) {}
    BinaryTreeNode* root;
};

BinaryTree::BinaryTree()
{
    d = new BinaryTreeData;
    QList<BinaryTreeNode*> nodesList;
    //check book page:119
    for(int i = 0; i < 11; i++)
    {
        BinaryTreeNode* node = new BinaryTreeNode("Node: " + QString::number(i + 1) + " ");
        nodesList.append(node);
    }
    // make the structure given in the book
    d->root = nodesList.at(0);
    d->root->left = nodesList.at(1);
    d->root->right = nodesList.at(2);

    BinaryTreeNode* node = nodesList.at(1);
    node->left = nodesList.at(3);
    node->right = nodesList.at(4);

    node = nodesList.at(2);
    node->left = nodesList.at(5);
    node->right = nodesList.at(6);

    node = nodesList.at(4);
    node->left = nodesList.at(7);
    node->right = nodesList.at(8);

    node = nodesList.at(5);
    node->left = nodesList.at(9);
    node->right = nodesList.at(10);

}

BinaryTreeNode *BinaryTree::root() const
{
    return d->root;
}

void BinaryTree::preOrderTraversal(BinaryTreeNode *node) const
{
    if(node)
    {
        node->print();
        preOrderTraversal(node->left);
        preOrderTraversal(node->right);
    }
}

void BinaryTree::inOrderTraversal(BinaryTreeNode *node) const
{
    if(node)
    {
        inOrderTraversal(node->left);
        node->print();
        inOrderTraversal(node->right);
    }
}

void BinaryTree::postOrderTraversal(BinaryTreeNode *node) const
{
    if(node)
    {
        postOrderTraversal(node->left);
        postOrderTraversal(node->right);
        node->print();
    }
}

void BinaryTree::levelOrderTraversal(BinaryTreeNode *node) const
{
    if(!node)
        return;

    QQueue<BinaryTreeNode*> nodeQueue;
    BinaryTreeNode* tempNode = 0;
    nodeQueue.append(node);
    while(!nodeQueue.isEmpty())
    {
        tempNode = nodeQueue.dequeue();
        tempNode->print();
        if(tempNode->left)
            nodeQueue.append(tempNode->left);

        if(tempNode->right)
            nodeQueue.append(tempNode->right);
    }
}

int BinaryTree::SizeOfBinaryTree(BinaryTreeNode* root) const
{
    if(!root)
        return 0;

    return (SizeOfBinaryTree(root->left) +  1 + SizeOfBinaryTree(root->right));
}

int BinaryTree::HeightOfTree(BinaryTreeNode *root) const
{
    int leftHeight = 0;
    int rightHeight = 0;
    if(!root)
        return 0;

    leftHeight = HeightOfTree(root->left);
    rightHeight = HeightOfTree(root->right);

    if(leftHeight > rightHeight)
        return leftHeight + 1;

    return rightHeight + 1;
}

