#ifndef BINARYTREE_H
#define BINARYTREE_H
#include <iostream>
#include <QString>

struct BinaryTreeNode
{
public:
    BinaryTreeNode(int val) : data(val), left(0), right(0) {}
    int data;
    BinaryTreeNode* left;
    BinaryTreeNode* right;

    void print() {
        std::cout << data;
    }
};

struct BinaryTreeData;
class BinaryTree
{
public:
    BinaryTree();
    BinaryTreeNode *root() const;

    void preOrderTraversal(BinaryTreeNode *node) const;
    void inOrderTraversal(BinaryTreeNode *node) const;
    void postOrderTraversal(BinaryTreeNode *node) const;
    void levelOrderTraversal(BinaryTreeNode* node) const;
    int SizeOfBinaryTree(BinaryTreeNode* root) const;
    int HeightOfTree(BinaryTreeNode* root) const;

    void highlightNode(BinaryTreeNode* node);
    BinaryTreeNode *highlightedNode() const;

    //Binary Search Tree Functions
    BinaryTreeNode* findInBST(int data);
    void insertInBST(int data);
    void deleteFromBST(int data);
private:
    BinaryTreeNode *findParentForNodeToInsert(BinaryTreeNode* root, int data);
    BinaryTreeNode *findNodeAndSetParent(BinaryTreeNode *root, int data, BinaryTreeNode** parentPtr);
    BinaryTreeNode *findNodeParentInLeft(BinaryTreeNode *root, BinaryTreeNode* node);
    BinaryTreeNode* deleteFromBST(BinaryTreeNode *root, int data);
    BinaryTreeNode *findMax(BinaryTreeNode * root) const;
    BinaryTreeNode* findMin(BinaryTreeNode *root) const;

private:
    BinaryTreeData* d;
};

#endif // BINARYTREE_H
