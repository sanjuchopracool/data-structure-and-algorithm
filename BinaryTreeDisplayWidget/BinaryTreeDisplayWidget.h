#ifndef BINARYTREEDISPLAYWIDGET_H
#define BINARYTREEDISPLAYWIDGET_H

#include <QWidget>
#include "BinaryTree.h"

class BinaryTreeDisplayWidget : public QWidget
{
    Q_OBJECT

public:
    BinaryTreeDisplayWidget(QWidget *parent = 0);
    ~BinaryTreeDisplayWidget();
    void setBinaryTree(BinaryTree* root);

protected:
    void paintEvent(QPaintEvent *ev);

private:
    void recursivePaintTree(BinaryTreeNode* node, QPainter* painter, QPoint pos, int widthParam);

private:
    BinaryTree* m_tree;
    int m_yUnitDistance;
    int m_heightOfTree;
    int m_nodeRadius;
};

#endif // BINARYTREEDISPLAYWIDGET_H
