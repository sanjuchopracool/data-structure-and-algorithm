#-------------------------------------------------
#
# Project created by QtCreator 2013-11-27T20:09:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BinaryTreeDisplayWidget
TEMPLATE = app


SOURCES += main.cpp\
        BinaryTreeDisplayWidget.cpp \
    BinaryTree.cpp

HEADERS  += BinaryTreeDisplayWidget.h \
    BinaryTree.h
