#include "BinaryTreeDisplayWidget.h"
#include <QPainter>
#include <QPaintEvent>
#include <QDebug>
#include <math.h>
#include "BinaryTree.h"
#define MAX_NODE_SIZE  30
BinaryTreeDisplayWidget::BinaryTreeDisplayWidget(QWidget *parent)
    : QWidget(parent)
{
    m_tree = 0;
    m_heightOfTree = 0;
    m_nodeRadius = 20;
}

BinaryTreeDisplayWidget::~BinaryTreeDisplayWidget()
{

}

void BinaryTreeDisplayWidget::setBinaryTree(BinaryTree *root)
{
    if(root)
    {
        m_tree = root;
        m_heightOfTree = m_tree->HeightOfTree(m_tree->root());
    }
}

void BinaryTreeDisplayWidget::paintEvent(QPaintEvent *ev)
{
    if(!m_tree)
        return;

    if(!m_heightOfTree)
        return;

    m_yUnitDistance = (this->height()- 6)/ m_heightOfTree;

    //calculate radius so that tree fit into it
    m_nodeRadius = this->width() / ( 2 * pow(2, m_heightOfTree -1));
    if(m_nodeRadius > MAX_NODE_SIZE)
        m_nodeRadius = MAX_NODE_SIZE;

    int halfHeight = m_yUnitDistance/2;
    if(halfHeight < m_nodeRadius)
        m_nodeRadius = halfHeight;

    int xDistance = this->width() / 4;

    QPoint point;
    point.setX(width() /2);
    point.setY(3 + m_nodeRadius);
    QPainter painter(this);
    painter.setBrush(Qt::gray);
    painter.setPen(QPen(Qt::black, 3));
    painter.setRenderHint(QPainter::Antialiasing, true);
    recursivePaintTree(m_tree->root(), &painter, point, xDistance);
    ev->accept();
}

void BinaryTreeDisplayWidget::recursivePaintTree(BinaryTreeNode *node, QPainter *painter, QPoint pos, int widthParam)
{
    if(!node || !painter)
        return;

    int x = pos.x();
    int y = pos.y();
    int halfWidth = widthParam / 2;
    BinaryTreeNode* highlightedNode = m_tree->highlightedNode();
    if(node->left)
    {
        QPoint lPos;

        lPos.setX(x - widthParam);
        lPos.setY(y + m_yUnitDistance);

        painter->save();
        if(node->left == highlightedNode)
            painter->setPen(QPen(Qt::red, 3));
        painter->drawLine(pos, lPos);
        painter->restore();
        recursivePaintTree(node->left, painter,lPos , halfWidth);
    }
    if(node->right)
    {
        QPoint rPos;
        rPos.setX(x + widthParam);
        rPos.setY(y + m_yUnitDistance);
        painter->save();
        if(node->right == highlightedNode)
            painter->setPen(QPen(Qt::red, 3));
        painter->drawLine(pos, rPos);
        painter->restore();

        recursivePaintTree(node->right, painter,rPos , halfWidth);
    }

    QRect rect;
    rect.setTopLeft(QPoint(x - m_nodeRadius, y - m_nodeRadius));
    rect.setWidth(2*m_nodeRadius);
    rect.setHeight(2*m_nodeRadius);
    painter->save();
    if(node == highlightedNode)
        painter->setPen(QPen(Qt::red, 3));

    painter->drawEllipse(rect);
    painter->setPen(Qt::white);
    QFont font = painter->font();
    font.setBold(true);
    font.setItalic(true);
    painter->setFont(font);
    painter->drawText(rect, Qt::AlignCenter ,QString::number(node->data));
    painter->restore();
}
