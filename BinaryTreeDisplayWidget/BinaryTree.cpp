#include <iostream>
#include "BinaryTree.h"
#include <QString>
#include <QList>
#include<QQueue>
using namespace std;

struct BinaryTreeData
{
    BinaryTreeData() : root(0), highlightedNode(0){}
    BinaryTreeNode* root;
    BinaryTreeNode* highlightedNode;
};

BinaryTree::BinaryTree()
{
    d = new BinaryTreeData;
//    QList<BinaryTreeNode*> nodesList;
//    //check book page:119
//    for(int i = 0; i < 11; i++)
//    {
//        BinaryTreeNode* node = new BinaryTreeNode(QString::number(i + 1) + " ");
//        nodesList.append(node);
//    }
    // make the structure given in the book
//    d->root = nodesList.at(0);
//    d->highlightedNode = d->root;
//    d->root->left = nodesList.at(1);
//    d->root->right = nodesList.at(2);

//    BinaryTreeNode* node = nodesList.at(1);
//    node->left = nodesList.at(3);
//    node->right = nodesList.at(4);

//    node = nodesList.at(2);
//    node->left = nodesList.at(5);
//    node->right = nodesList.at(6);

//    node = nodesList.at(4);
//    node->left = nodesList.at(7);
//    node->right = nodesList.at(8);

//    node = nodesList.at(5);
//    node->left = nodesList.at(9);
//    node->right = nodesList.at(10);
//    d->highlightedNode = node->right;

}

BinaryTreeNode *BinaryTree::root() const
{
    return d->root;
}

void BinaryTree::preOrderTraversal(BinaryTreeNode *node) const
{
    if(node)
    {
        node->print();
        preOrderTraversal(node->left);
        preOrderTraversal(node->right);
    }
}

void BinaryTree::inOrderTraversal(BinaryTreeNode *node) const
{
    if(node)
    {
        inOrderTraversal(node->left);
        node->print();
        inOrderTraversal(node->right);
    }
}

void BinaryTree::postOrderTraversal(BinaryTreeNode *node) const
{
    if(node)
    {
        postOrderTraversal(node->left);
        postOrderTraversal(node->right);
        node->print();
    }
}

void BinaryTree::levelOrderTraversal(BinaryTreeNode *node) const
{
    if(!node)
        return;

    QQueue<BinaryTreeNode*> nodeQueue;
    BinaryTreeNode* tempNode = 0;
    nodeQueue.append(node);
    while(!nodeQueue.isEmpty())
    {
        tempNode = nodeQueue.dequeue();
        tempNode->print();
        if(tempNode->left)
            nodeQueue.append(tempNode->left);

        if(tempNode->right)
            nodeQueue.append(tempNode->right);
    }
}

int BinaryTree::SizeOfBinaryTree(BinaryTreeNode* root) const
{
    if(!root)
        return 0;

    return (SizeOfBinaryTree(root->left) +  1 + SizeOfBinaryTree(root->right));
}

int BinaryTree::HeightOfTree(BinaryTreeNode *root) const
{
    int leftHeight = 0;
    int rightHeight = 0;
    if(!root)
        return 0;

    leftHeight = HeightOfTree(root->left);
    rightHeight = HeightOfTree(root->right);

    if(leftHeight > rightHeight)
        return leftHeight + 1;

    return rightHeight + 1;
}

void BinaryTree::highlightNode(BinaryTreeNode *node)
{
    if(node)
        d->highlightedNode = node;
}

BinaryTreeNode *BinaryTree::highlightedNode() const
{
    return d->highlightedNode;
}

BinaryTreeNode* BinaryTree::findInBST(int data)
{
    BinaryTreeNode* root = d->root;
    while(root)
    {
        if(data < root->data)
            root = root->left;
        else if(data > root->data)
            root = root->right;
        else
            return root;
    }

    return root;
}

void BinaryTree::insertInBST(int data)
{
    if(!d->root)
        d->root = new BinaryTreeNode(data);
    else
    {
        BinaryTreeNode* node = findParentForNodeToInsert(d->root, data);
        if(node)
        {
            if(node->data == data)
                return;

            BinaryTreeNode* nodeToAdd = new BinaryTreeNode(data);
            if(data < node->data)
                node->left = nodeToAdd;
            else if(data > node->data)
                node->right = nodeToAdd;

        }
    }
}

void BinaryTree::deleteFromBST(int data)
{
    if(!d->root)
        return;

    deleteFromBST(d->root, data);
}

BinaryTreeNode *BinaryTree::findNodeAndSetParent(BinaryTreeNode *root,int data, BinaryTreeNode **parentPtr)
{
    BinaryTreeNode* rootNode = root;
    *parentPtr = NULL;
    while(rootNode && rootNode->data != data)
    {
        *parentPtr = rootNode;
        if(data < rootNode->data)
            rootNode = rootNode->left;
        else if(data > rootNode->data)
            rootNode = rootNode->right;
    }

    return rootNode;
}

BinaryTreeNode *BinaryTree::findNodeParentInLeft(BinaryTreeNode *root,BinaryTreeNode *node)
{
    BinaryTreeNode* rootNode = root->left;
    BinaryTreeNode* parentPtr = root;
    while(rootNode && rootNode != node)
    {
        parentPtr = rootNode;
        if(node->data < rootNode->data)
            rootNode = rootNode->left;
        else if(node->data > rootNode->data)
            rootNode = rootNode->right;
    }

    return parentPtr;
}

BinaryTreeNode* BinaryTree::deleteFromBST(BinaryTreeNode *root, int data)
{
    BinaryTreeNode* parentNode;
    BinaryTreeNode* nodeToDelete = findNodeAndSetParent(root, data, &parentNode);
    if(nodeToDelete)
    {
        if(parentNode)
        {
            bool isLeftNode = parentNode->left == nodeToDelete;
            if(nodeToDelete->left && nodeToDelete->right)
            {
                BinaryTreeNode* nodeToReplace = findMax(nodeToDelete->left);
                if(nodeToReplace)
                {
                    nodeToDelete->data = nodeToReplace->data;
                    nodeToDelete->left = deleteFromBST(nodeToDelete->left, nodeToReplace->data);
                    return root;
                }
            }
            if(!nodeToDelete->left && !nodeToDelete->right)
            {
                if(parentNode->left == nodeToDelete)
                    parentNode->left = NULL;
                else if(parentNode->right == nodeToDelete)
                    parentNode->right = NULL;
            }
            else if(nodeToDelete->left)
            {
                if(isLeftNode)
                    parentNode->left = nodeToDelete->left;
                else
                    parentNode->right = nodeToDelete->left;

            }
            else if(nodeToDelete->right)
            {
                if(isLeftNode)
                    parentNode->left = nodeToDelete->right;
                else
                    parentNode->right = nodeToDelete->right;

            }
            delete nodeToDelete;
        }
        else
        {
            if(!root->left && !root->right)
            {
                delete root;
                return NULL;
            }
            else
            {
                BinaryTreeNode* nodeToReplace = findMax(nodeToDelete->left);
                if(nodeToReplace)
                {
                    nodeToDelete->data = nodeToReplace->data;
                    nodeToDelete->left = deleteFromBST(nodeToDelete->left, nodeToReplace->data);
                    return root;
                }
            }
        }

    }
    return root;
}

BinaryTreeNode* BinaryTree::findParentForNodeToInsert(BinaryTreeNode *root, int data)
{
    BinaryTreeNode* rootNode = root;
    BinaryTreeNode* prev = NULL;
    while(rootNode)
    {
        prev = rootNode;
        if(data < rootNode->data)
            rootNode = rootNode->left;
        else if(data > rootNode->data)
            rootNode = rootNode->right;
        else
            return rootNode;
    }

    return prev;
}

BinaryTreeNode *BinaryTree::findMax(BinaryTreeNode *root) const
{
    if(!root)
        return NULL;
    if(root->right == NULL)
        return root;
    else
        return findMax(root->right);
}

BinaryTreeNode *BinaryTree::findMin(BinaryTreeNode *root) const
{
    if(!root)
        return NULL;
    if(root->left == NULL)
        return root;
    else
        return findMax(root->left);
}

