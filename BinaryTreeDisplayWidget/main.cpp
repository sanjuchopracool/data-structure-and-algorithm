#include "BinaryTreeDisplayWidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    BinaryTreeDisplayWidget w;
    BinaryTree tree;
    tree.insertInBST(5);
    tree.insertInBST(2);
    tree.insertInBST(-2);
    tree.insertInBST(9);
    tree.insertInBST(6);
    tree.insertInBST(3);
    tree.insertInBST(-23);
    tree.insertInBST(53);
    tree.insertInBST(12);
    tree.insertInBST(-52);
    tree.insertInBST(23);
    tree.insertInBST(-1);
    tree.insertInBST(0);
    w.setBinaryTree(&tree);
    w.show();
    tree.deleteFromBST(-1);
    tree.deleteFromBST(-23);
    tree.deleteFromBST(-2);
    tree.deleteFromBST(2);
    return a.exec();
}
